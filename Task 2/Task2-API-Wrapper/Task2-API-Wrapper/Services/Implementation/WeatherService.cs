﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Task2_API_Wrapper.Helpers.HttpClient;

namespace Task2_API_Wrapper.Services.Implementation
{
    public class WeatherService : IWeatherService
    {
        private readonly IHttpClientHelper _httpClientHelper;
        private readonly ILogger _logger;

        public WeatherService(IHttpClientHelper httpClientHelper, ILogger<WeatherService> logger)
        {
            _httpClientHelper = httpClientHelper;
            _logger = logger;
        }

        public async Task<Temperatures> GetWeatherByLocationId(string locationId)
        {
            _logger.LogDebug("Get data by location with location Id {locationId}", locationId);

            string requestUrl = @$"{Constants.MetaWeatherEndPoint}{locationId}";
            var result = await _httpClientHelper.GetAsync<Temperatures>(requestUrl);

            _logger.LogDebug("Get data by location successful");

            return result;
        }
    }
}
