﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task2_API_Wrapper.Services
{
    public interface IWeatherService
    {
         Task<Temperatures> GetWeatherByLocationId(string locationId);
    }
}
