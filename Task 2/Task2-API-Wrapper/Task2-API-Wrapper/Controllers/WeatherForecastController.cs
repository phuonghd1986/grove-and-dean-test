﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Task2_API_Wrapper.Services;

namespace Task2_API_Wrapper.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IWeatherService _weatherService;
        private readonly ILogger<WeatherForecastController> _logger;
        public WeatherForecastController(IWeatherService weatherService, ILogger<WeatherForecastController> logger)
        {
            _weatherService = weatherService;
            _logger = logger;
        }

        [HttpGet("GetByLocation/{locationId}")]
        public async Task<IActionResult> Get([FromRoute] string locationId)
        {
            if (string.IsNullOrEmpty(locationId))
            {
                _logger.LogDebug("Missing or Invalid Location Id");
                _logger.LogError("Request with Missing or Invalid Location Id");
                return BadRequest("Missing or Invalid Location Id");
            }

            _logger.LogDebug("Get weather by location with Id {locationId}", locationId);

            var result = await _weatherService.GetWeatherByLocationId(locationId);

            _logger.LogDebug("Get weather by location successful.");
            return Ok(result);

        }
    }
}
