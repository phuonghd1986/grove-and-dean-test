﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Task2_API_Wrapper.Helpers.HttpClient
{
    public class HttpClientHelper : IHttpClientHelper
    {
        public  async Task<T> GetAsync<T>(string url)
        {
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {
                using (HttpResponseMessage response = await client.GetAsync(url))
                using (HttpContent content = response.Content)
                {
                    string dataRead = await content.ReadAsStringAsync();
                    if (dataRead != null)
                    {
                        var data = JsonConvert.DeserializeObject<T>(dataRead);
                        return (T)data;
                    }
                }
            }

            var o = new Object();
            return (T)o;
        }

        public  async Task<T> PostAsync<T>(string url, HttpContent contentPost)
        {
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {
                using (HttpResponseMessage response = await client.PostAsync(url, contentPost))
                using (HttpContent content = response.Content)
                {
                    string dataRead = await content.ReadAsStringAsync();
                    if (dataRead != null)
                    {
                        var data = JsonConvert.DeserializeObject<T>(dataRead);
                        return (T)data;
                    }
                }
            }

            var o = new Object();
            return (T)o;
        }
    }
}
