﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Task2_API_Wrapper.Helpers.HttpClient
{
    public interface IHttpClientHelper
    {
        Task<T> GetAsync<T>(string url);
        Task<T> PostAsync<T>(string url, HttpContent contentPost);
    }
}
