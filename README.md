# Grove and Dean  Test
This is guidline how to run these apps

##Task 1:

### 1. Pre-requires:
- Install nodejs v10.x or above. Recommend latest version at link https://nodejs.org/en/
- Install yarn follow this link https://classic.yarnpkg.com/en/docs/install#windows-stable
- Install vue-cli:
	+ run commnad:  npm install -g @vue/cli	

### 2. How to run:
- Go to folder: Task 1\task1-vue then open command line terminal
- Run: yarn install
- Run: yarn yarn serve 
- After run successfull open link: http://localhost:8080/ to access webpage

##Task 2:

### 1. Pre-requires:
- .Net core SDK version 3.1 or above
- Visual studio 2019 or visual studio code

### 2. How to run:
- Go to folder: Task 2\Task2-API-Wrapper
- Open Task2.API.Wrapper.sln by visual studio or visual studio code
- Build the project to ensure it compiled successfull
- Run the project
- Access the API using these sample link as below for testing:
	+ http://localhost:5000/weatherforecast/getbylocation/1252431
    + http://localhost:5000/weatherforecast/getbylocation/862592


##Task 4:

### 1. Pre-requires:
- Visual studio 2019 or visual studio code

### 2. How to run:
- Just open the solution file project at Task 4\Test.Task4.CodeRefactoring\Test.Task4.CodeRefactoring.sln
- This project can not compile, just refactored structure

Done

If you have any concern or issue please contact me: huynhducphuong@gmail.com. Thank you