﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Task4.CodeRefactoring.Infrastructure
{
    public class Constants
    {
        public static string[] CountyList =
        {
            "",
            "Nordland",
            "Nord Trøndelag",
            "Sør Trøndelag", 
            "Møre og Romsdal", 
            "Sogn og Fjordane",
            "Hordaland",
            "Rogaland", "Vest Agder"
        };
    }
}