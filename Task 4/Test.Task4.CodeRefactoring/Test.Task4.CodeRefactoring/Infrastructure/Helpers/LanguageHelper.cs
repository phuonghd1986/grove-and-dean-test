﻿namespace Test.Task4.CodeRefactoring.Infrastructure.Helpers
{
    public class LanguageHelper
    {
        public static string GetLanguageString(string xmlPath)
        {
            return EPiServer.Core.LanguageManager.Instance.Translate(xmlPath, GetCurrentLanguage());
        }

        public static string GetCurrentLanguage()
        {
            return EPiServer.Globalization.ContentLanguage.PreferredCulture.Name;
        }
    }
}