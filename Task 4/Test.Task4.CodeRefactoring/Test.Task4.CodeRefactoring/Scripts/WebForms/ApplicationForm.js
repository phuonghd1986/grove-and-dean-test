﻿/*

 Code logic for Application Form
*/

$(document).ready(function () {
    SI.Files.stylizeAll();

    scrollToFirstError();

    $('.formSubmitter').bind('click', function () {
        scrollToFirstError();
    });

    $('#applicationForm input[type=file]').change(function () {
        var text = $(this).val();
        $(this).parent().siblings('.filename').text($(this).val());
    });
});

function scrollToFirstError() {
    $.each($('.warning'), function () {
        if ($(this).css('visibility') != 'hidden') {
            $.scrollTo($(this));
            return false;
        }
    });
}