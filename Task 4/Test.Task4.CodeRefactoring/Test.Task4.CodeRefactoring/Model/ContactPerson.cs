﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Task4.CodeRefactoring.Model
{
    public class ContactPerson
    {
        public ContactPerson(string municipality, string county, string email)
        {
            Municipality = municipality;
            County = county;
            Email = email;
        }

        public string Municipality { get; set; }
        public string County { get; set; }
        public string Email { get; set; }
    }

}