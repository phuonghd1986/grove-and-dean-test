﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Legacy.Core.PageTypes;
using Legacy.Core.Services;
using Legacy.Web.Templates.Base;
using Legacy.Web.Utilities;
using Test.Task4.CodeRefactoring;
using Test.Task4.CodeRefactoring.Infrastructure;
using Test.Task4.CodeRefactoring.Infrastructure.Helpers;
using Test.Task4.CodeRefactoring.Model;
using Test.Task4.CodeRefactoring.Services;

namespace Legacy.Web.Templates.Pages
{
    public partial class ApplicationForm : TemplatePageBase<ApplicationFormPage>
    {
        protected List<ContactPerson> contactPersonList;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
            {
                DataBind();
                PopulateCountyList();
            }
        }

        protected bool SendFormContentByEmail()
        {
            string subject = PropertyService.GetStringProperty(CurrentPage, "EmailSubject");
            string content = BuildEmailContent();
            string applicationReciever = GetEmailForMunicipality(Ddl_Municipality.SelectedValue);
            string applicationSender = Txt_Email.Text;

            MailMessage mailMessage = EmailHelper.BuildMail(applicationSender, subject, content, applicationReciever, applicationReciever, GetAttachments());
            return EmailHelper.SendMail(mailMessage, true);
        }

        #region Fill GUI controls
        /// <summary>
        /// Populates the County dropDownList
        /// </summary>
        protected void PopulateCountyList()
        {
            Ddl_County.DataSource = Constants.CountyList;
            Ddl_County.DataBind();
        }

        /// <summary>
        /// Populate Ddl_Municipality with municipality from the given county
        /// </summary>
        /// <param name="county"></param>
        protected void PopulateMunicipalityList(string county)
        {
            if (contactPersonList == null || contactPersonList.Count == 0)
            {
                PopulateContactPersonList();
            }

            Ddl_Municipality.Items.Clear();
            Ddl_Municipality.Items.Add(new ListItem("", ""));

            foreach (ContactPerson contactPerson in contactPersonList)
            {
                if (contactPerson.County.Equals(county))
                {
                    if (contactPerson.Municipality == "mrHeroy")
                    {
                        Ddl_Municipality.Items.Add(new ListItem("Herøy", contactPerson.Municipality));
                    }
                    else
                    {
                        Ddl_Municipality.Items.Add(new ListItem(contactPerson.Municipality));
                    }
                    
                }
            }
        }

        /// <summary>
        /// Creates as many FileUpload controls as configured on the page.
        /// </summary>
        private void BuildDynamicControls()
        {
            if (pnlFileUpload.Visible)
            {
                //Create dummy datasource to display the correct number of FileUpload controls.
                if (!CurrentPage.Property["NumberOfFileUploads"].IsNull)
                {
                    int numberOfFiles = (int)CurrentPage.Property["NumberOfFileUploads"].Value;

                    if (numberOfFiles > 0)
                    {
                        List<int> list = new List<int>();
                        for (int i = 0; i < numberOfFiles; i++)
                        {
                            list.Add(i);
                        }

                        rptFileUpload.DataSource = list;
                        rptFileUpload.DataBind();
                    }
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Attachement button clicked
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        protected void btnShowFileUpload_Click(object sender, EventArgs e)
        {
            pnlFileUpload.Visible = true;
            BuildDynamicControls();
            btnShowFileUpload.Visible = false;
        }

        /// <summary>
        /// Submit button clicked
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">e</param>
        protected void Btn_SubmitForm_Click(object sender, EventArgs e)
        {
            // Server side validation, if javascript is disabled
            Page.Validate();
            if (Page.IsValid)
            {
                if (SendFormContentByEmail())
                {
                    string receiptUrl = PropertyService.GetPageDataPropertyLinkUrl(CurrentPage, "FormReceiptPage");
                    Response.Redirect(receiptUrl);
                }
                else
                {
                    string errorUrl = PropertyService.GetPageDataPropertyLinkUrl(CurrentPage, "FormErrorPage");
                    Response.Redirect(errorUrl);
                }
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the Ddl_County control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Ddl_County_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!Ddl_County.SelectedValue.Equals(string.Empty))
            {
                PopulateMunicipalityList(Ddl_County.SelectedValue);
            }
            else
            {
                Ddl_Municipality.Items.Clear();
                Ddl_Municipality.DataBind();
            }
        }

        #endregion

        #region Email handling
        
        /// <summary>
        /// Returns a list of selected Attachments
        /// </summary>
        /// <returns></returns>
        private Attachment[] GetAttachments()
        {
            List<Attachment> attachmentList = new List<Attachment>();

            foreach (string postedInputName in Request.Files)
            {
                var postedFile = Request.Files[postedInputName];

                if (postedFile != null && postedFile.ContentLength > 0)
                {
                    string fileName = Path.GetFileName(postedFile.FileName);
                    if (fileName != string.Empty)
                    {
                        Attachment newAttachment = new Attachment(postedFile.InputStream, fileName, postedFile.ContentType);
                        attachmentList.Add(newAttachment);
                    }
                }
            }

            return attachmentList.ToArray();
        }

        /// <summary>
        /// Builds the content of the email body
        /// </summary>
        /// <returns></returns>
        protected string BuildEmailContent()
        {
            const string SummaryStart = "<table>";
            const string SummaryEnd = "</table>";
            const string ContentStart = "<html>";
            const string ContentEnd = "</html>";
            const string LabelElementStart = "<tr><td><strong>";
            const string LabelElementEnd = "</strong></td>";
            const string ValueElementStart = "<td>";
            const string ValueElementEnd = "</td></tr>";
            const string LabelElementFullWidthStart = "<tr><td colspan=\"2\"><strong>";
            const string LabelElementFullWidthEnd = "</strong></td></tr>";
            const string ValueElementFullWidthStart = "<tr><td colspan=\"2\">";
            const string ValueElementFullWidthEnd = "</td></tr>";

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(ContentStart);
            stringBuilder.AppendLine(PropertyService.GetStringProperty(CurrentPage, "EmailHeader"));
            stringBuilder.AppendLine(SummaryStart);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/county") + LabelElementEnd + ValueElementStart + Ddl_County.SelectedValue + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/municipality") + LabelElementEnd + ValueElementStart + Ddl_Municipality.SelectedItem + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/applicator") + LabelElementEnd + ValueElementStart + Txt_Applicator.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/address") + LabelElementEnd + ValueElementStart + Txt_Address.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/postcode") + " / " + LanguageHelper.GetLanguageString("/applicationform/postarea") + LabelElementEnd + ValueElementStart + Txt_PostCode.Text + " " + Txt_PostArea.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/orgnobirthnumber") + LabelElementEnd + ValueElementStart + Txt_OrgNoBirthNumber.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/contactperson") + LabelElementEnd + ValueElementStart + Txt_ContactPerson.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/phone") + LabelElementEnd + ValueElementStart + Txt_Phone.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/email") + LabelElementEnd + ValueElementStart + Txt_Email.Text + ValueElementEnd);
            stringBuilder.AppendLine(LabelElementFullWidthStart + LanguageHelper.GetLanguageString("/applicationform/description") + LabelElementFullWidthEnd + ValueElementFullWidthStart + Txt_Description.Text + ValueElementFullWidthEnd);
            stringBuilder.AppendLine(LabelElementFullWidthStart + LanguageHelper.GetLanguageString("/applicationform/financeplan") + LabelElementFullWidthEnd + ValueElementFullWidthStart + Txt_FinancePlan.Text + ValueElementFullWidthEnd);
            stringBuilder.AppendLine(LabelElementFullWidthStart + LanguageHelper.GetLanguageString("/applicationform/businessdescription") + LabelElementFullWidthEnd + ValueElementFullWidthStart + Txt_BusinessDescription.Text + ValueElementFullWidthEnd);
            stringBuilder.AppendLine(LabelElementStart + LanguageHelper.GetLanguageString("/applicationform/applicationAmount") + LabelElementEnd + ValueElementStart + Txt_ApplicationAmount.Text + ValueElementEnd);
            stringBuilder.AppendLine(SummaryEnd);
            stringBuilder.AppendLine(PropertyService.GetStringProperty(CurrentPage, "EmailFooter"));
            stringBuilder.AppendLine(ContentEnd);

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Gets the email address or the contact person for provided municipality (kommune)
        /// </summary>
        /// <param name="municipality"></param>
        /// <returns></returns>
        protected string GetEmailForMunicipality(string municipality)
        {
            if (contactPersonList == null || contactPersonList.Count == 0)
            {
                PopulateContactPersonList();
            }

            foreach (ContactPerson contactPerson in contactPersonList)
            {
                if (contactPerson.Municipality.Equals(municipality, StringComparison.InvariantCultureIgnoreCase))
                {
                    return contactPerson.Email;
                }
            }

            return null;
        }

        #endregion

        protected void PopulateContactPersonList()
        {            
            contactPersonList = ContactPersonService.Instance.GetContactPersonList();
        }
      
    }
}